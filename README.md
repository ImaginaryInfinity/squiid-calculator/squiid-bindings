# Squiid Bindings

[![codecov](https://codecov.io/gl/ImaginaryInfinity:squiid-calculator/squiid-bindings/graph/badge.svg?token=2YVK2PWQFF)](https://codecov.io/gl/ImaginaryInfinity:squiid-calculator/squiid-bindings)

This repository contains the source code for Squiid engine and Squiid parser language bindings.

## Squiid Engine:

| Language | Path                                                             | Package (if applicable)                                                                                  |
| -------- | ---------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------- |
| Python   | [bindings/python/squiid_engine](./bindings/python/squiid_engine) | [![PyPI - Version](https://img.shields.io/pypi/v/squiid-engine)](https://pypi.org/project/squiid-engine) |
| C        | [bindings/c/squiid_engine](./bindings/c/squiid_engine/)          |                                                                                                          |
| C++      | [bindings/c/squiid_engine](./bindings/c/squiid_engine/)          |                                                                                                          |

## Squiid Parser:

| Language | Path                                                             | Package (if applicable)                                                                                  |
| -------- | ---------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------- |
| Python   | [bindings/python/squiid_parser](./bindings/python/squiid_parser) | [![PyPI - Version](https://img.shields.io/pypi/v/squiid-parser)](https://pypi.org/project/squiid-parser) |
| C        | [bindings/c/squiid_parser](./bindings/c/squiid_parser/)          |                                                                                                          |
| C++      | [bindings/c/squiid_parser](./bindings/c/squiid_parser/)          |                                                                                                          |
