/**
 * @file squiid_engine.hpp
 * @author Connor Sample
 * @date 1/12/2025
 *
 * @brief C++ bindings to the Squiid engine
 *
 * @section LICENSE
 *
 * Squiid engine C++ bindings
 * Copyright (C) 2025  Connor Sample
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _SQUIID_ENGINE_HPP_
#define _SQUIID_ENGINE_HPP_

#include "squiid_engine.h"
#include <string>
#include <vector>

namespace squiid_engine {
  enum BucketTypes {
    FLOAT = 1,
    STRING,
    CONSTANT,
    UNDEFINED,
  };

  enum ConstantTypes {
    PI = 1,
    HALF_PI,
    THIRD_PI,
    QUARTER_PI,
    SIXTH_PI,
    EIGHTH_PI,
    TWO_PI,
    E,
    C,
    G,
    PHI,
  };

  /**
   * @brief A Bucket contains an item that can be on the stack.
   */
  class Bucket {
  public:
    Bucket(std::string value, BucketTypes bucket_type,
           ConstantTypes constant_type);

    /**
     * @value String representation of a value
     */
    std::string value;
    /**
     * @bucket_type The type of value
     */
    BucketTypes bucket_type;
    /**
     * @constant_type If `bucket_type` is a constant, this contains the specific
     * constant
     */
    ConstantTypes constant_type;

    static Bucket from_ffi(BucketFFI &ffi_bucket);

    bool operator==(const Bucket &other) const;
    bool operator!=(const Bucket &other) const;
  };

  /**
   * @brief Struct to identify which EngineSignals were triggered during the
   * submission of multiple commands to the engine.
   */
  class EngineSignalSet {
  public:
    EngineSignalSet(bool stack_updated, bool quit, std::string error);

    bool stack_updated();
    bool should_quit();
    bool has_error();
    std::string get_error();

    static EngineSignalSet from_ffi(EngineSignalSetFFI &ffi_signal);

    bool operator==(const EngineSignalSet &other) const;
    bool operator!=(const EngineSignalSet &other) const;

  private:
    bool _stack_updated;
    bool _quit;
    std::string _error;
  };

  /**
   * @brief Class used to access the Squiid engine.
   */
  class SquiidEngine {
  public:
    SquiidEngine();

    EngineSignalSet execute_multiple_rpn(std::vector<std::string> &data);

    EngineSignalSet execute_single_rpn(const std::string &data);

    std::vector<Bucket> get_stack();

    std::vector<std::string> get_commands();

    Bucket get_previous_answer();

    EngineSignalSet update_previous_answer();
  };

} // namespace squiid_engine

#endif
