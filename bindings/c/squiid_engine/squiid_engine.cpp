/**
 * @file squiid_engine.cpp
 * @author Connor Sample
 * @date 2/19/2025
 *
 * @brief C++ bindings to the Squiid engine
 *
 * @section LICENSE
 *
 * Squiid engine C++ bindings
 * Copyright (C) 2025  Connor Sample
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
#include "squiid_engine.hpp"
#include "squiid_engine.h"
#include <stdexcept>

namespace {
  /**
   * @brief Convert a `BucketTypesFFI` to a `BucketTypes`.
   *
   * @param[in] ffi_type a `BucketTypesFFI` to convert
   */
  [[nodiscard]]
  squiid_engine::BucketTypes bucketTypesFromFFI(BucketTypesFFI ffi_type) {
    switch (ffi_type) {
    case Float:
      return squiid_engine::BucketTypes::FLOAT;
    case String:
      return squiid_engine::BucketTypes::STRING;
    case Constant:
      return squiid_engine::BucketTypes::CONSTANT;
    case Undefined:
      return squiid_engine::BucketTypes::UNDEFINED;
    // LCOV_EXCL_START
    default:
      throw std::invalid_argument("Unknown BucketTypesFFI value");
      // LCOV_EXCL_STOP
    }
  }

  /**
   * @brief Convert a `ConstantTypesFFI` to a `ConstantTypes`.
   *
   * @param[in] ffi_constant a `ConstantTypesFFI` to convert
   */
  [[nodiscard]]
  squiid_engine::ConstantTypes
  constantTypesFromFFI(ConstantTypesFFI ffi_constant) {
    switch (ffi_constant) {
    case Pi:
      return squiid_engine::ConstantTypes::PI;
    case HalfPi:
      return squiid_engine::ConstantTypes::HALF_PI;
    case ThirdPi:
      return squiid_engine::ConstantTypes::THIRD_PI;
    case QuarterPi:
      return squiid_engine::ConstantTypes::QUARTER_PI;
    case SixthPi:
      return squiid_engine::ConstantTypes::SIXTH_PI;
    case EighthPi:
      return squiid_engine::ConstantTypes::EIGHTH_PI;
    case TwoPi:
      return squiid_engine::ConstantTypes::TWO_PI;
    case E:
      return squiid_engine::ConstantTypes::E;
    case C:
      return squiid_engine::ConstantTypes::C;
    case G:
      return squiid_engine::ConstantTypes::G;
    case Phi:
      return squiid_engine::ConstantTypes::PHI;
    // LCOV_EXCL_START
    default:
      throw std::invalid_argument("Unkown ConstantTypesFFI value");
      // LCOV_EXCL_STOP
    }
  }
} // namespace

namespace squiid_engine {

  /**
   * @brief Construct a new Bucket.
   *
   * @param[in] value a string representing the bucket's value
   * @param[in] bucket_type the type of the bucket showing what the type of
   * `value` is
   * @param[in] constant_type if `bucket_type` is a constant, this is the type
   * of constant
   */
  Bucket::Bucket(std::string value, BucketTypes bucket_type,
                 ConstantTypes constant_type)
      : value(std::move(value)), bucket_type(bucket_type),
        constant_type(constant_type) {}

  /**
   * @brief Construct a `Bucket` from a `BucketFFI`
   *
   * @param[in] ffi_bucket a `BucketFFI` struct
   * @return a new Bucket
   */
  [[nodiscard]]
  Bucket Bucket::from_ffi(BucketFFI &ffi_bucket) {
    std::string value = ffi_bucket.value ? std::string(ffi_bucket.value) : "";
    BucketTypes bt = bucketTypesFromFFI(ffi_bucket.bucket_type);
    ConstantTypes ct = constantTypesFromFFI(ffi_bucket.constant_type);

    return Bucket(std::move(value), bt, ct);
  }

  EngineSignalSet::EngineSignalSet(bool stack_updated, bool quit,
                                   std::string error)
      : _stack_updated(stack_updated), _quit(quit), _error(error) {}

  bool Bucket::operator==(const Bucket &other) const {
    return value == other.value && bucket_type == other.bucket_type &&
           constant_type == other.constant_type;
  }

  bool Bucket::operator!=(const Bucket &other) const {
    return !Bucket::operator==(other);
  }

  /**
   * @brief Construct an `EngineSignalSet` from an `EngineSignalSetFFI`
   *
   * @param[in] ffi_signal an `EngineSignalSetFFI` struct
   * @return A new EngineSignalSet
   */
  [[nodiscard]]
  EngineSignalSet EngineSignalSet::from_ffi(EngineSignalSetFFI &ffi_signal) {
    std::string error = ffi_signal.error ? std::string(ffi_signal.error) : "";

    return EngineSignalSet(ffi_signal.stack_updated, ffi_signal.quit, error);
  }

  /**
   * @brief Check if the stack has been updated and should be retrieved.
   *
   * @return whether or not the stack is updated
   */
  [[nodiscard]]
  bool EngineSignalSet::stack_updated() {
    return _stack_updated;
  }

  /**
   * @brief Check if a quit was requested.
   *
   * @details This means that the user requested that the engine quit, and thus
   * the frontend should also quit.
   *
   * @return whether or not a quit was requested
   */
  [[nodiscard]]
  bool EngineSignalSet::should_quit() {
    return _quit;
  }

  /**
   * @brief Check whether or not there is an error present.
   *
   * @return whether or not there is an error
   */
  [[nodiscard]]
  bool EngineSignalSet::has_error() {
    return !_error.empty();
  }

  /**
   * @brief Get the error that is present, if applicable.
   *
   * @return The error if it exists, or an empty string
   */
  [[nodiscard]]
  std::string EngineSignalSet::get_error() {
    return _error;
  }

  bool EngineSignalSet::operator==(const EngineSignalSet &other) const {
    return _stack_updated == other._stack_updated && _quit == other._quit &&
           _error == other._error;
  }

  bool EngineSignalSet::operator!=(const EngineSignalSet &other) const {
    return !EngineSignalSet::operator==(other);
  }

  /**
   * @brief Construct a new SquiidEngine
   */
  SquiidEngine::SquiidEngine() {
    // check if binding is compatible with library version
    // this cant really be tested
    // LCOV_EXCL_START
    char *error = NULL;
    if (!check_compatible(COMPATIBLE_ENGINE_VERSION, &error)) {
      std::string error_copy = "no error message provided";

      if (error != NULL) {
        error_copy = std::string(error);
      }

      free_string(error);
      throw std::runtime_error(error_copy);
    }
    // LCOV_EXCL_STOP
  }

  /**
   * @brief Execute multiple RPN commands in the engine at once.
   *
   * @param[in] data list of RPN commands as strings. Example: ["3", "3",
   * "add"]
   * @return which engine signals were triggered from the commands
   */
  [[nodiscard]]
  EngineSignalSet
  SquiidEngine::execute_multiple_rpn(std::vector<std::string> &data) {
    std::vector<const char *> cstrings;
    for (const auto &str : data) {
      cstrings.push_back(str.c_str());
    }

    EngineSignalSetFFI signal_set_ffi =
        execute_multiple_rpn_exposed(cstrings.data(), cstrings.size());

    auto messages = EngineSignalSet::from_ffi(signal_set_ffi);

    free_engine_signal_set(signal_set_ffi);

    return messages;
  }

  /**
   * @brief Execute a single RPN statement.
   *
   * @param[in] data the single command to execute
   * @return Which Engine Signals were triggered from the commands
   * @throws runtime_error if fetching the stack from the engine fails
   */
  [[nodiscard]]
  EngineSignalSet SquiidEngine::execute_single_rpn(const std::string &data) {
    std::vector<std::string> data_vec = {data};
    return execute_multiple_rpn(data_vec);
  }

  /**
   * @brief Get the engine's current stack.
   *
   * @return the stack
   * @throws runtime_error if, for some reason, the stack was unable to be
   * fetched
   */
  [[nodiscard]]
  std::vector<Bucket> SquiidEngine::get_stack() {
    int out_len = 0;
    BucketFFI **stack_ffi = get_stack_exposed(&out_len);

    if (!stack_ffi) {
      throw std::runtime_error(                    // LCOV_EXCL_LINE
          "failed to retrieve stack from engine"); // LCOV_EXCL_LINE
    }

    std::vector<Bucket> stack;
    for (int i = 0; i < out_len; i++) {
      if (stack_ffi[i]) {
        stack.push_back(Bucket::from_ffi(*stack_ffi[i]));
      }
    }

    free_bucket_array(stack_ffi, out_len);

    return stack;
  }

  /**
   * @brief Get a list of valid commands that the engine accepts.
   *
   * @return list of commands from the engine
   * @throws runtime_error if fetching the commands from the engine fails
   */
  [[nodiscard]]
  std::vector<std::string> SquiidEngine::get_commands() {
    int out_len = 0;

    char **commands_ffi = get_commands_exposed(&out_len);
    if (!commands_ffi) {
      throw std::runtime_error(                       // LCOV_EXCL_LINE
          "failed to retrieve commands from engine"); // LCOV_EXCL_LINE
    }

    std::vector<std::string> commands;

    for (int i = 0; i < out_len; i++) {
      if (commands_ffi[i]) {
        commands.push_back(std::string(commands_ffi[i]));
      }
    }

    free_string_array(commands_ffi, out_len);

    return commands;
  }

  /**
   * @brief Get the current previous answer variable from the engine.
   *
   * @return Bucket containing the value of the previous answer.
   * @throws runtime_error if fetching the previous answer from the engine fails
   */
  [[nodiscard]]
  Bucket SquiidEngine::get_previous_answer() {
    BucketFFI *prev_ans_ffi = get_previous_answer_exposed();
    if (!prev_ans_ffi) {
      throw std::runtime_error(                  // LCOV_EXCL_LINE
          "failed to retrieve previous answer"); // LCOV_EXCL_LINE
    }

    Bucket prev_ans = Bucket::from_ffi(*prev_ans_ffi);

    free_bucket(prev_ans_ffi);

    return prev_ans;
  }

  /**
   * @brief Update the previous answer variable in the engine.
   *
   * @details This should be called after a full algebraic statement in
   algebraic mode, or after each RPN command if in RPN mode.
   *
   * @return an EngineSignalSet containing the potential error encountered while
   updating the previous answer
   */
  [[nodiscard]]
  EngineSignalSet SquiidEngine::update_previous_answer() {
    EngineSignalSetFFI ffi_signals = update_previous_answer_exposed();

    EngineSignalSet signals = EngineSignalSet::from_ffi(ffi_signals);

    free_engine_signal_set(ffi_signals);

    return signals;
  }

} // namespace squiid_engine
