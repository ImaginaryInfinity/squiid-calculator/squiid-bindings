#include "../squiid_engine.hpp"
#include <gtest/gtest.h>

TEST(BucketConstructionTest, Float) {
  char val[] = "3";
  BucketFFI bucket_ffi =
      BucketFFI{val, BucketTypesFFI::Float, ConstantTypesFFI::Pi};

  auto converted_bucket = squiid_engine::Bucket::from_ffi(bucket_ffi);
  ASSERT_EQ(converted_bucket.value, "3");
  ASSERT_EQ(converted_bucket.bucket_type, squiid_engine::BucketTypes::FLOAT);
  ASSERT_EQ(converted_bucket.constant_type, squiid_engine::ConstantTypes::PI);
}

TEST(BucketConstructionTest, Constant) {
  char val[] = "#e";
  BucketFFI bucket_ffi =
      BucketFFI{val, BucketTypesFFI::Constant, ConstantTypesFFI::E};

  auto converted_bucket = squiid_engine::Bucket::from_ffi(bucket_ffi);
  ASSERT_EQ(converted_bucket.value, "#e");
  ASSERT_EQ(converted_bucket.bucket_type, squiid_engine::BucketTypes::CONSTANT);
  ASSERT_EQ(converted_bucket.constant_type, squiid_engine::ConstantTypes::E);
}

TEST(BucketConstructionTest, String) {
  char val[] = "fuzzy";
  BucketFFI bucket_ffi =
      BucketFFI{val, BucketTypesFFI::String, ConstantTypesFFI::Pi};

  auto converted_bucket = squiid_engine::Bucket::from_ffi(bucket_ffi);
  ASSERT_EQ(converted_bucket.value, "fuzzy");
  ASSERT_EQ(converted_bucket.bucket_type, squiid_engine::BucketTypes::STRING);
  ASSERT_EQ(converted_bucket.constant_type, squiid_engine::ConstantTypes::PI);
}

TEST(BucketConstructionTest, Undefined) {
  char val[] = "";
  BucketFFI bucket_ffi =
      BucketFFI{val, BucketTypesFFI::Undefined, ConstantTypesFFI::Pi};

  auto converted_bucket = squiid_engine::Bucket::from_ffi(bucket_ffi);
  ASSERT_EQ(converted_bucket.value, "");
  ASSERT_EQ(converted_bucket.bucket_type,
            squiid_engine::BucketTypes::UNDEFINED);
  ASSERT_EQ(converted_bucket.constant_type, squiid_engine::ConstantTypes::PI);
}

TEST(BucketEquality, Basic) {
  auto lhs =
      squiid_engine::Bucket("testing", squiid_engine::BucketTypes::STRING,
                            squiid_engine::ConstantTypes::PI);

  auto rhs =
      squiid_engine::Bucket("testing", squiid_engine::BucketTypes::STRING,
                            squiid_engine::ConstantTypes::PI);

  auto rhs_ne =
      squiid_engine::Bucket("testing1", squiid_engine::BucketTypes::STRING,
                            squiid_engine::ConstantTypes::PI);

  auto rhs_ne_2 =
      squiid_engine::Bucket("testing", squiid_engine::BucketTypes::CONSTANT,
                            squiid_engine::ConstantTypes::PI);

  auto rhs_ne_3 =
      squiid_engine::Bucket("testing", squiid_engine::BucketTypes::STRING,
                            squiid_engine::ConstantTypes::E);

  ASSERT_EQ(lhs, rhs);
  ASSERT_NE(lhs, rhs_ne);
  ASSERT_NE(lhs, rhs_ne_2);
  ASSERT_NE(lhs, rhs_ne_3);
}

TEST(EngineSignalSetConstructionTest, StackUpdated) {
  char err[] = "";
  EngineSignalSetFFI ess_ffi = EngineSignalSetFFI{true, false, err};

  auto converted_ess = squiid_engine::EngineSignalSet::from_ffi(ess_ffi);
  ASSERT_TRUE(converted_ess.stack_updated());
  ASSERT_FALSE(converted_ess.should_quit());
  ASSERT_FALSE(converted_ess.has_error());
  ASSERT_EQ(converted_ess.get_error(), "");
}

TEST(EngineSignalSetConstructionTest, ShouldQuit) {
  char err[] = "";
  EngineSignalSetFFI ess_ffi = EngineSignalSetFFI{false, true, err};

  auto converted_ess = squiid_engine::EngineSignalSet::from_ffi(ess_ffi);
  ASSERT_FALSE(converted_ess.stack_updated());
  ASSERT_TRUE(converted_ess.should_quit());
  ASSERT_FALSE(converted_ess.has_error());
  ASSERT_EQ(converted_ess.get_error(), "");
}

TEST(EngineSignalSetConstructionTest, HasError) {
  char err[] = "this is an error";
  EngineSignalSetFFI ess_ffi = EngineSignalSetFFI{false, false, err};

  auto converted_ess = squiid_engine::EngineSignalSet::from_ffi(ess_ffi);
  ASSERT_FALSE(converted_ess.stack_updated());
  ASSERT_FALSE(converted_ess.should_quit());
  ASSERT_TRUE(converted_ess.has_error());
  ASSERT_EQ(converted_ess.get_error(), "this is an error");
}

TEST(EngineSignalSetEquality, Basic) {
  auto lhs = squiid_engine::EngineSignalSet(true, false, "testing");
  auto rhs = squiid_engine::EngineSignalSet(true, false, "testing");
  auto rhs_ne = squiid_engine::EngineSignalSet(false, false, "testing");
  auto rhs_ne_2 = squiid_engine::EngineSignalSet(true, true, "testing");
  auto rhs_ne_3 = squiid_engine::EngineSignalSet(true, false, "testing2");

  ASSERT_EQ(lhs, rhs);
  ASSERT_NE(lhs, rhs_ne);
  ASSERT_NE(lhs, rhs_ne_2);
  ASSERT_NE(lhs, rhs_ne_3);
}

// test conversion of constants
#define CONSTANT_CONVERSION_TEST(uppercase_name, pascalcase_name)              \
  TEST(ConstantTypeConversion, uppercase_name) {                               \
    char data[] = "";                                                          \
    auto ffi = BucketFFI{data, BucketTypesFFI::Undefined,                      \
                         ConstantTypesFFI::pascalcase_name};                   \
    auto bucket = squiid_engine::Bucket::from_ffi(ffi);                        \
    ASSERT_EQ(bucket.constant_type,                                            \
              squiid_engine::ConstantTypes::uppercase_name);                   \
  }

CONSTANT_CONVERSION_TEST(PI, Pi)
CONSTANT_CONVERSION_TEST(HALF_PI, HalfPi)
CONSTANT_CONVERSION_TEST(THIRD_PI, ThirdPi)
CONSTANT_CONVERSION_TEST(QUARTER_PI, QuarterPi)
CONSTANT_CONVERSION_TEST(SIXTH_PI, SixthPi)
CONSTANT_CONVERSION_TEST(EIGHTH_PI, EighthPi)
CONSTANT_CONVERSION_TEST(TWO_PI, TwoPi)
CONSTANT_CONVERSION_TEST(E, E)
CONSTANT_CONVERSION_TEST(C, C)
CONSTANT_CONVERSION_TEST(G, G)
CONSTANT_CONVERSION_TEST(PHI, Phi)
