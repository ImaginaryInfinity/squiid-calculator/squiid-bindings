#include "../squiid_engine.hpp"
#include <algorithm>
#include <gtest/gtest.h>

class EngineTest : public ::testing::Test {
protected:
  void SetUp() override {
    auto e = squiid_engine::SquiidEngine();
    e.execute_single_rpn("clear");
  }
};

TEST_F(EngineTest, SubmitMultipleCommands) {
  auto e = squiid_engine::SquiidEngine();

  std::vector<std::string> data = {"3", "5", "7", "multiply", "add"};
  auto res = e.execute_multiple_rpn(data);

  ASSERT_FALSE(res.has_error());

  auto stack = e.get_stack();
  ASSERT_EQ(stack[0].value, "38");
  ASSERT_EQ(stack[0].bucket_type, squiid_engine::BucketTypes::FLOAT);
}

TEST_F(EngineTest, SubmitSingleCommands) {
  auto e = squiid_engine::SquiidEngine();

  (void)e.execute_single_rpn("3");
  (void)e.execute_single_rpn("4");
  auto res = e.execute_single_rpn("add");

  ASSERT_FALSE(res.has_error());

  auto stack = e.get_stack();
  ASSERT_EQ(stack[0].value, "7");
  ASSERT_EQ(stack[0].bucket_type, squiid_engine::BucketTypes::FLOAT);
}

TEST_F(EngineTest, GetEmptyStack) {
  auto e = squiid_engine::SquiidEngine();

  ASSERT_TRUE(e.get_stack().empty());
}

TEST_F(EngineTest, GetCommands) {
  auto e = squiid_engine::SquiidEngine();

  auto commands = e.get_commands();

  ASSERT_GT(commands.size(), 10);

  // check for presence of add command
  int count = std::count(commands.begin(), commands.end(), "add");
  ASSERT_EQ(count, 1);
}

TEST_F(EngineTest, PreviousAnswer) {
  auto e = squiid_engine::SquiidEngine();
  //
  std::vector<std::string> data = {"3", "4", "5", "multiply", "add"};
  e.execute_multiple_rpn(data);

  auto res = e.update_previous_answer();
  ASSERT_FALSE(res.has_error());

  ASSERT_EQ(e.get_previous_answer().value, "23");

  std::vector<std::string> data2 = {"12", "6", "divide"};
  (void)e.execute_multiple_rpn(data2);

  // make sure it hasnt changed yet
  ASSERT_EQ(e.get_previous_answer().value, "23");
  ASSERT_FALSE(e.update_previous_answer().has_error());
  ASSERT_EQ(e.get_previous_answer().value, "2");
}
