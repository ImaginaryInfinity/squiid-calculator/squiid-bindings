# C/C++ Bindings

[![codecov](https://codecov.io/gl/ImaginaryInfinity:squiid-calculator/squiid-bindings/graph/badge.svg?token=2YVK2PWQFF)](https://codecov.io/gl/ImaginaryInfinity:squiid-calculator/squiid-bindings)

This directory contains the bindings for C and C++. The C bindings are contained within the [`squiid_engine.h`](./squiid_engine.h) file. The C++ bindings are a combination of [`squiid_engine.h`](./squiid_engine.h), [`squiid_engine.hpp`](./squiid_engine.hpp), and [`squiid_engine.cpp`](./squiid_engine.cpp).

## Demos

Demos of how to compile C and C++ projects using the bindings with `make` and `cmake` can be found in the `demo/` directory. Read through the README in the `demo/C` or `demo/CPP` for more information on how to build and run the demo applications.

Since the Makefiles aren't statically compiling, you'll have to add the directory containing `libsquiid_engine.so` to your `$LD_LIBRARY_PATH`. If this is your current directory, you can run the binary with `LD_LIBRARY_PATH=. ./squiid_demo`
