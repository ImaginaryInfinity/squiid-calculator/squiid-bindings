#include "squiid/include/squiid_engine.h"
#include <assert.h>
#include <stdio.h>

int main() {
  assert(check_compatible(COMPATIBLE_ENGINE_VERSION, NULL));

  const char *thing[] = {"3", "3", "add"};
  execute_multiple_rpn_exposed(thing, 3);

  int stack_len = 0;
  BucketFFI **stack = get_stack_exposed(&stack_len);

  for (int i = 0; i < stack_len; i++) {
    printf("%s\n", stack[i]->value);
  }

  free_bucket_array(stack, stack_len);
  return 0;
}
