#include "squiid/include/squiid_engine.hpp"
#include <cstdio>
#include <iostream>
using namespace std;

int main() {
  squiid_engine::SquiidEngine engine;
  vector<string> commands = {"1", "1", "add"};
  cout << "Executing 1+1..." << endl;

  auto response = engine.execute_multiple_rpn(commands);
  // update the previous answer variable in the engine
  // note that this isnt used in this example, but you should be doing this
  // after each full equation
  engine.update_previous_answer();

  cout << "Stack updated: " << response.stack_updated() << endl << endl;

  cout << "Stack:" << endl;
  auto stack = engine.get_stack();
  for (squiid_engine::Bucket item : stack) {
    cout << item.value << endl;
  }

  cout << endl;

  cout << "Multiplying previous answer by pi" << endl;
  engine.execute_single_rpn("#pi");
  engine.execute_single_rpn("multiply");

  stack = engine.get_stack();
  for (squiid_engine::Bucket item : stack) {
    cout << item.value << endl;
    if (item.constant_type == squiid_engine::ConstantTypes::TWO_PI) {
      cout << "TwoPi" << endl;
    } else {
      cerr << "something has gone terribly wrong" << endl;
    }
  }

  return 0;
}
