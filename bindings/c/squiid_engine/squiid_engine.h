/**
 * @file squiid_engine.h
 * @author Connor Sample
 * @date 2/19/2025
 *
 * @brief C bindings to the Squiid engine
 *
 * @section LICENSE
 *
 * Squiid engine C bindings
 * Copyright (C) 2025  Connor Sample
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _SQUIID_ENGINE_H_
#define _SQUIID_ENGINE_H_

#define COMPATIBLE_ENGINE_VERSION ">=2.1.0,<3.0"

#ifdef __cplusplus
#include <cstdarg>
#include <cstdint>
#include <cstdlib>
#else
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#endif

/**
 * enum BucketTypesFFI - An enum defining types of data stored in a Bucket.
 */
typedef enum BucketTypesFFI {
  Float = 1,
  String,
  Constant,
  Undefined,
} BucketTypesFFI;

/**
 * enum ConstantTypesFFI - An enum defining types of constants stored in a
 * Bucket if the BucketTypes value is CONSTANT.
 */
typedef enum ConstantTypesFFI {
  Pi = 1,
  HalfPi,
  ThirdPi,
  QuarterPi,
  SixthPi,
  EighthPi,
  TwoPi,
  E,
  C,
  G,
  Phi,
} ConstantTypesFFI;

/**
 * struct EngineSignalSetFFI - Struct to identify which EngineSignals were
 * triggered during the submission of multiple commands to the engine.
 */
typedef struct EngineSignalSetFFI {
  /**
   * Whether or not the frontend should fetch the stack
   */
  bool stack_updated;
  /**
   * Whether or not the frontend should quit
   */
  bool quit;
  /**
   * This is set if an error was encountered, or null if not
   */
  char *error;
} EngineSignalSetFFI;

/**
 * struct BucketFFI - A Bucket contains an item that can be on the stack.
 */
typedef struct BucketFFI {
  /**
   * Bucket value. Will be null when undefined
   */
  char *value;
  /**
   * The type of the Bucket
   */
  enum BucketTypesFFI bucket_type;
  /**
   * The type of the constant if bucket_type is Constant, else will be Pi
   */
  enum ConstantTypesFFI constant_type;
} BucketFFI;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief FFI-Exposed function to submit multiple RPN commands to the engine.
 *
 * @param[in] rpn_data the RPN data array of strings to execute
 * @param[in] rpn_data_length the length of `rpn_data`
 *
 * @return EngineSignalSetFFI containing which engine Signals were triggered
 * from the commands
 */
struct EngineSignalSetFFI
execute_multiple_rpn_exposed(const char *const *rpn_data,
                             uintptr_t rpn_data_length);

/**
 * @brief Submit a single RPN command to the engine.
 *
 * @param[in] rpn_data the RPN command as a string to execute
 * @return EngineSignalSetFFI containing which engine Signals were triggered
 * from the command
 */
static inline struct EngineSignalSetFFI
execute_single_rpn_exposed(const char *rpn_data) {
  // construct 1-length array with RPN data
  const char *rpn_array[1] = {rpn_data};

  return execute_multiple_rpn_exposed(rpn_array, 1);
}

/**
 * @brief Get the engine's current stack.
 *
 * @param[out] outlen A pointer to an integer to store the length of the output
 * array
 *
 * @return An array of BucketFFI structs (the stack)
 */
struct BucketFFI **get_stack_exposed(int *outlen);

/**
 * @brief Get the engine's list of currently supported commands.
 *
 * @param[out] outlen A pointer to an integer to store the length of the output
 * array
 *
 * @return An array of commands as strings that the engine supports.
 */
char **get_commands_exposed(int *outlen);

/**
 * @brief Get the current previous answer from the engine.
 *
 * @return BucketFFI containing the value of the previous answer
 */
struct BucketFFI *get_previous_answer_exposed(void);

/**
 * @brief Update the previous answer variable in the engine.
 *
 * @details This should be called after a full algebraic statement in algebraic
 * mode, or after each RPN command if in RPN mode.
 *
 * @return EngineSignalSetFF containing an error if one was encountered while
 * updating the previous answer
 */
struct EngineSignalSetFFI update_previous_answer_exposed(void);

/**
 * @brief Check that a binding is compatible with this version of the library.
 *
 * @param[in] version_constraint the version constraint that the binding is
 * compatible with. See the `semver` crate for the format
 * @param[out] expected_version_out reference to a string that can be written to
 * for error messages, or NULL to ignore errors
 * @return whether the constrained version matches the library version
 */
bool check_compatible(const char *version_constraint,
                      char **expected_version_out);

/**
 * @brief Free the error string contained within the EngineSignalSetFFI struct
 *
 * @param[in] ptr Pointer to a EngineSignalSetFFI struct which was returned from
 * Rust
 */
void free_engine_signal_set(struct EngineSignalSetFFI ptr);

/**
 * @brief Free an array of strings that was returned over the FFI boundary.
 * @details Panics if the vec or string are invalid data
 *
 * @param[in] array the string array to free
 * @param[in] len the length of the string array
 */
void free_string_array(char **array, int len);

/**
 * @brief Free a string that was returned over the FFI boundary.
 * @details Panics if the string is invalid data
 *
 * @param[in] the string to free
 */
void free_string(char *string);

/**
 * @brief Free an array of Bucket objects that was returned over the FFI
 * boundary.
 * @details Panics if the array pointer is null or if the vec or Bucket are
 * invalid data
 *
 * @param[in] array the bucket array to free
 * @param[in] len the length of the bucket array
 */
void free_bucket_array(struct BucketFFI **array, int len);

/**
 * @brief Free a Bucket object that was returned over the FFI boundary.
 * @details Panics if the bucket pointer is null or if the bucket is invalid
 * data
 *
 * @param[in] bucket_ffi The Bucket to free
 */
void free_bucket(struct BucketFFI *bucket_ffi);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // define _SQUIID_ENGINE_H_
