# C/C++ Bindings

[![codecov](https://codecov.io/gl/ImaginaryInfinity:squiid-calculator/squiid-bindings/graph/badge.svg?token=2YVK2PWQFF)](https://codecov.io/gl/ImaginaryInfinity:squiid-calculator/squiid-bindings)

This directory contains the bindings for C and C++. The C bindings are contained within the [`squiid_parser.h`](./squiid_parser.h) file. The C++ bindings are a combination of [`squiid_parser.h`](./squiid_parser.h), [`squiid_parser.hpp`](./squiid_parser.hpp), and [`squiid_parser.cpp`](./squiid_parser.cpp).

## Demos

Demos of how to compile C and C++ projects using the bindings with `make` can be found in the `demo/` directory. Note that `libsquiid_parser.so` must be present in the `demo/` directory to compile. I would also recommend reading through the comments in the Makefile if you get stuck. You can choose which Makefile to run with `make -f Makefile_C` or `make -f Makefile_CPP`.

Since the Makefiles aren't statically compiling, you'll have to add the directory containing `libsquiid_parser.so` to your `$LD_LIBRARY_PATH`. If this is your current directory, you can run the binary with `LD_LIBRARY_PATH=. ./squiid_demo`
