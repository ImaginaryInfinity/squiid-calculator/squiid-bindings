/**
 * @file squiid_parser.cpp
 * @author Connor Sample
 * @date 2/19/2025
 *
 * @brief C++ bindings to the Squiid parser
 *
 * @section LICENSE
 *
 * Squiid parser C++ bindings
 * Copyright (C) 2025  Connor Sample
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
#include "squiid_parser.hpp"
#include "squiid_parser.h"
#include <cassert>
#include <cstring>

namespace squiid_parser {
  /**
   * @brief Construct a new SquiidParser
   */
  SquiidParser::SquiidParser() {
    // check if binding is compatible with library version
    // this cant really be tested
    // LCOV_EXCL_START
    char *error = NULL;
    if (!check_compatible(COMPATIBLE_PARSER_VERSION, &error)) {
      std::string error_copy = "no error message provided";

      if (error != NULL) {
        error_copy = std::string(error);
      }

      free_string(error);
      throw std::runtime_error(error_copy);
    }
    // LCOV_EXCL_STOP
  }

  /**
   * @brief Parse an algebraic notation string into an RPN notation list of
   * strings.
   *
   * @param[in] input_string The algebraic string to parse.
   *
   * @return The list of operating strings in RPN notation.
   * @throws runtime_error if the parser returns an error
   */
  [[nodiscard]]
  std::vector<std::string> SquiidParser::parse(std::string &input_string) {
    const char *cstring = input_string.c_str();

    ParseResultFFI result = parse_exposed(cstring);

    if (result.error != nullptr) {
      // error was encountered
      throw std::runtime_error(result.error);
    }

    // since the error is null, the result should be set
    assert(result.result != nullptr);

    std::vector<std::string> ret_arr(result.result,
                                     result.result + result.result_len);

    // free parse result
    free_parse_result(result);

    return ret_arr;
  }
} // namespace squiid_parser
