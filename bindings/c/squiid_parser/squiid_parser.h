/**
 * @file squiid_parser.h
 * @author Connor Sample
 * @date 2/19/2025
 *
 * @brief C bindings to the Squiid parser
 *
 * @section LICENSE
 *
 * Squiid parser C bindings
 * Copyright (C) 2025  Connor Sample
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _SQUIID_PARSER_H_
#define _SQUIID_PARSER_H_

#define COMPATIBLE_PARSER_VERSION ">=1.4.0,<2.0"

#ifdef __cplusplus
#include <cstdarg>
#include <cstdint>
#include <cstdlib>
#include <new>
#include <ostream>
#else
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#endif

/**
 * Structure containing the result of a parse operation done over FFI. Will
 * contain either a result array or an error message, but not both.
 */
typedef struct ParseResultFFI {
  /**
   * The array of strings if the result was a success, else null
   */
  char **result;
  /**
   * The length of the result array
   */
  int result_len;
  /**
   * The error message if an error was encountered, else null
   */
  char *error;
} ParseResultFFI;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Parse a given algebraic (infix) notation string into an array of RPN
 * (postfix) commands.
 *
 * @param[in] input The string input to parse
 *
 * @return A struct containing the result or error of a parse operation.
 */
ParseResultFFI parse_exposed(const char *input);

/**
 * @brief Check that a binding is compatible with this version of the library.
 *
 * @param[in] version_constraint the version constraint that the binding is
 * compatible with. See the `semver` crate for the format
 * @param[out] expected_version_out reference to a string that can be written to
 * for error messages, or NULL to ignore errors
 * @return whether the constrained version matches the library version
 */
bool check_compatible(const char *version_constraint,
                      char **expected_version_out);

/**
 * @brief Free an array of strings that was returned over the FFI boundary.
 *
 * @details Panics if the array pointer is null or if the vec or strings are
 * invalid data
 *
 * @param[in] array The string array to free
 * @param[in] len The length of the string array
 */
void free_parse_result(ParseResultFFI parse_result);

/**
 * @brief Free a string that was returned over the FFI boundary.
 * @details Panics if the string is invalid data
 *
 * @param[in] the string to free
 */
void free_string(char *string);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // define _SQUIID_PARSER_H_
