# Squiid Parser C Demo

This directory contains a demo `Makefile` and `CMakeLists.txt` file for using the Squiid Parser C bindings in your project. You can delete whichever you're not using. Below lists the important files:

- `squiid/lib/` - This is where you place the `libsquiid_parser.so`, `libsquiid_parser.dylib`, or `squiid_parser.dll`.
- `squiid/include/squiid_parser.h` - The header file defining the exports of the bindings
- `Makefile` - An example file for projects using make
- `CMakeLists.txt` - An example file for projects using cmake
- `demo.c` - The main application that uses the bindings

## Building With Make

Run `make` in this directory to build the project with make

## Building with CMake

Run `mkdir build && cd build`, then `cmake ..` followed by `make` to build the project with cmake.

## Error while loading shared libraries

Since the Makefiles aren't statically compiling, you'll have to add the directory containing `libsquiid_parser.so` to your `$LD_LIBRARY_PATH`. If running from this directory, that would be `LD_LIBRARY_PATH=squiid/lib/ ./squiid_demo`.
