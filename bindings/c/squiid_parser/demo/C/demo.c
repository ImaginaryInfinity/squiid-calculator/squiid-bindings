#include "squiid/include/squiid_parser.h"
#include <assert.h>
#include <stdio.h>

int main() {
  assert(check_compatible(COMPATIBLE_PARSER_VERSION, NULL));

  char *string = "(3+5)*6/(4-2)";
  ParseResultFFI result = parse_exposed(string);

  if (result.error != NULL) {
    fprintf(stderr, "Error while parsing: %s\n", result.error);
    return -1;
  }

  for (int i = 0; i < result.result_len; i++) {
    printf("%s\n", result.result[i]);
  }

  free_parse_result(result);

  return 0;
}
