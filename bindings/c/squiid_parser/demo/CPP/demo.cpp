#include "squiid/include/squiid_parser.hpp"
#include <iostream>
#include <string>
#include <vector>

int main() {
  std::string algebraic = "(3+5)*6/(4-2)";
  std::vector<std::string> parsed;

  squiid_parser::SquiidParser parser = squiid_parser::SquiidParser();

  // catch potential runtime_error
  try {
    parsed = parser.parse(algebraic);
  } catch (const std::runtime_error &e) {
    std::cerr << "Error while parsing algebraic string: " << e.what()
              << std::endl;
    return 1;
  }

  // print result to stdout
  for (auto str : parsed) {
    std::cout << str << std::endl;
  }

  return 0;
}
