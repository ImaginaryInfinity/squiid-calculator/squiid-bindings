/**
 * @file squiid_parser.hpp
 * @author Connor Sample
 * @date 2/19/2025
 *
 * @brief C++ bindings to the Squiid parser
 *
 * @section LICENSE
 *
 * Squiid parser C++ bindings
 * Copyright (C) 2025  Connor Sample
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _SQUIID_PARSER_HPP_
#define _SQUIID_PARSER_HPP_

#include <string>
#include <vector>

namespace squiid_parser {
  class SquiidParser {
  public:
    SquiidParser();

    std::vector<std::string> parse(std::string &input_string);
  };
} // namespace squiid_parser

#endif // define _SQUIID_PARSER_HPP_
