#include "../squiid_parser.hpp"
#include <gtest/gtest.h>

TEST(BasicParsingTest, Simple) {
  squiid_parser::SquiidParser parser = squiid_parser::SquiidParser();
  auto input_data = std::string("(3+5)*6");
  auto result = parser.parse(input_data);

  std::vector<std::string> correct = {"3", "5", "+", "6", "*"};
  EXPECT_EQ(result, correct);
}

TEST(BasicParsingTest, Complex) {
  squiid_parser::SquiidParser parser = squiid_parser::SquiidParser();
  auto input_data = std::string(
      "sqrt(5*(((((1+0.2*(350/"
      "661.5)^2)^3.5-1)*(1-(6.875*10^-6)*25500)^-5.2656)+1)^0.286-1))");
  auto result = parser.parse(input_data);

  std::vector<std::string> correct = {
      "5",   "1",   "0.2", "350",   "661.5", "/", "2",      "^",   "*",
      "+",   "3.5", "^",   "1",     "-",     "1", "6.875",  "10",  "6",
      "chs", "^",   "*",   "25500", "*",     "-", "5.2656", "chs", "^",
      "*",   "1",   "+",   "0.286", "^",     "1", "-",      "*",   "sqrt",
  };
  EXPECT_EQ(result, correct);
}

TEST(BasicParsingTest, Simple2) {
  squiid_parser::SquiidParser parser = squiid_parser::SquiidParser();
  auto input_data = std::string("8#pi*6(3-2)");
  auto result = parser.parse(input_data);

  std::vector<std::string> correct = {"8", "#pi", "*", "6", "*",
                                      "3", "2",   "-", "*"};
  EXPECT_EQ(result, correct);
}

TEST(BasicParsingTest, BadData) {
  squiid_parser::SquiidParser parser = squiid_parser::SquiidParser();
  auto input_data = std::string("\\~~``");
  EXPECT_THROW({ parser.parse(input_data); }, std::runtime_error);
}
