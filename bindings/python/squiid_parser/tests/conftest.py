import sys
from pathlib import Path

sys.path.insert(0, str(Path(__file__).parents[1].resolve()))

import build


def pytest_sessionstart(session):
    """Build the shared object file and place it in the appropriate directory."""
    build.build_shared_object()
