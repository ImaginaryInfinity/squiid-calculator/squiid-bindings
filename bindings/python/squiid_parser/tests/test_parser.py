import os
import sys
from pathlib import Path

import pytest

sys.path.insert(0, str(Path(__file__).parents[1].resolve()))

import squiid_parser as sp
from squiid_parser._data_structs import ParserError


def test_parse():
    """Test that basic parsing works as expected."""
    p = sp.SquiidParser()
    result = p.parse("(3+5)*6")

    assert result == ["3", "5", "+", "6", "*"]

    result = p.parse(
        "sqrt(5*(((((1+0.2*(350/661.5)^2)^3.5-1)*(1-(6.875*10^-6)*25500)^-5.2656)+1)^0.286-1))",
    )

    assert result == [
        "5",
        "1",
        "0.2",
        "350",
        "661.5",
        "/",
        "2",
        "^",
        "*",
        "+",
        "3.5",
        "^",
        "1",
        "-",
        "1",
        "6.875",
        "10",
        "6",
        "chs",
        "^",
        "*",
        "25500",
        "*",
        "-",
        "5.2656",
        "chs",
        "^",
        "*",
        "1",
        "+",
        "0.286",
        "^",
        "1",
        "-",
        "*",
        "sqrt",
    ]

    result = p.parse("8#pi*6(3-2)")
    assert result == ["8", "#pi", "*", "6", "*", "3", "2", "-", "*"]


def test_mismatched_version_number():
    """Test an error is thrown when the bindings aren't compatible with the library."""
    version_save: bytes = sp._backend.COMPATIBLE_PARSER_VERSION  # pyright: ignore[reportUnknownVariableType, reportUnknownMemberType, reportAttributeAccessIssue] # noqa: SLF001
    sp._backend.COMPATIBLE_PARSER_VERSION = b"<0"  # pyright: ignore[reportUnknownMemberType, reportAttributeAccessIssue] # noqa: SLF001
    with pytest.raises(RuntimeError):
        _ = sp.SquiidParser()

    sp._backend.COMPATIBLE_PARSER_VERSION = version_save  # pyright: ignore[reportUnknownMemberType, reportAttributeAccessIssue] # noqa: SLF001


def test_parse_bad_data():
    """Test data that is invalid to parse."""
    p = sp.SquiidParser()

    with pytest.raises(ParserError) as e:
        _ = p.parse("\\~~``")

    assert "Unexpected token" in str(e.value)


def test_load_specific_path():
    """Test if a dynamic library with can be loaded with a direct path."""
    directory = Path(__file__).parents[1] / "squiid_parser"
    direct_path: Path | None = None

    for file in directory.iterdir():
        if any(file.match(ext) for ext in ["*.so", "*.dll", "*.dylib"]):
            direct_path = file
            break

    assert direct_path is not None, "Shared object file not found"
    assert direct_path.is_file(), f"Shared object file not found at {direct_path}"

    # this should not crash
    _ = sp.SquiidParser(direct_path)


def test_load_invalid_path():
    """Test error handling with invalid dynamic library path."""
    bad_filename = Path(os.urandom(32).hex())

    assert not bad_filename.is_file(), f"file {bad_filename} exists"

    with pytest.raises(FileNotFoundError):
        _ = sp.SquiidParser(bad_filename)
