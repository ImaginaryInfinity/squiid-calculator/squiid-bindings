import squiid_parser

p = squiid_parser.SquiidParser()
result = p.parse("(3+5*7)")

assert result is not None
assert result == ["3", "5", "7", "*", "+"]

# squiid currently doesn't support symbols, but rather commands
# iterate through the list and convert symbols to commands
for index, value in enumerate(result):
    if value == "*":
        result[index] = "multiply"
    elif value == "+":
        result[index] = "add"
    elif value == "-":
        result[index] = "subtract"
    elif value == "/":
        result[index] = "divide"
    elif value == "^":
        result[index] = "power"
    elif value == "%":
        result[index] = "mod"

assert result == ["3", "5", "7", "multiply", "add"]

# and now pass this array to the engine...
