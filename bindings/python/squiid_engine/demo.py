import squiid_engine

e = squiid_engine.SquiidEngine()
# this accepts an RPN array
# if you want to accept algebraic input, you can use squiid-parser to accomplish this
res = e.execute_multiple_rpn(["3", "5", "7", "multiply", "add"])

# this should be called after each full expression is run
_ = e.update_previous_answer()

assert not res.has_error()

stack = e.get_stack()
assert stack[0].value == "38"
assert stack[0].bucket_type == squiid_engine.BucketTypes.FLOAT
