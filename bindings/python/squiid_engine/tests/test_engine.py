import os
import sys
from ctypes import c_int
from pathlib import Path

import pytest

sys.path.insert(0, str(Path(__file__).parents[1].resolve()))


import squiid_engine as se
from squiid_engine._data_structs import (
    Bucket_FFI,
    BucketTypes,
    ConstantTypes,
    EngineSignalSet_FFI,
)


@pytest.fixture(autouse=True)
def clean_stack() -> None:
    """Clear the stack before each run since state is shared."""
    e = se.SquiidEngine()
    _ = e.execute_single_rpn("clear")


def test_load_specific_path():
    """Test if a dynamic library with can be loaded with a direct path."""
    directory = Path(__file__).parents[1] / "squiid_engine"
    direct_path: Path | None = None

    for file in directory.iterdir():
        if any(file.match(ext) for ext in ["*.so", "*.dll", "*.dylib"]):
            direct_path = file
            break

    assert direct_path is not None, "Shared object file not found"
    assert direct_path.is_file(), f"Shared object file not found at {direct_path}"

    # this should not crash
    _ = se.SquiidEngine(direct_path)


def test_mismatched_version_number():
    """Test an error is thrown when the bindings aren't compatible with the library."""
    version_save: bytes = se._backend.COMPATIBLE_ENGINE_VERSION  # pyright: ignore[reportUnknownVariableType, reportUnknownMemberType, reportAttributeAccessIssue] # noqa: SLF001
    se._backend.COMPATIBLE_ENGINE_VERSION = b"<0"  # pyright: ignore[reportUnknownMemberType, reportAttributeAccessIssue] # noqa: SLF001
    with pytest.raises(RuntimeError):
        _ = se.SquiidEngine()

    se._backend.COMPATIBLE_ENGINE_VERSION = version_save  # pyright: ignore[reportUnknownMemberType, reportAttributeAccessIssue] # noqa: SLF001


def test_load_invalid_path():
    """Test error handling with invalid dynamic library path."""
    bad_filename = Path(os.urandom(32).hex())

    assert not bad_filename.is_file(), f"file {bad_filename} exists"

    with pytest.raises(FileNotFoundError):
        _ = se.SquiidEngine(bad_filename)


def test_bucket_construction():
    """Test that buckets can be correctly converted from Bucket_FFI structs."""
    num_bucket = se.Bucket.from_ffi(Bucket_FFI(b"3", c_int(1), c_int(1)))

    assert num_bucket.value == "3"
    assert num_bucket.bucket_type == BucketTypes.FLOAT
    assert num_bucket.constant_type == ConstantTypes.PI

    const_bucket = se.Bucket.from_ffi(Bucket_FFI(b"#e", c_int(3), c_int(8)))

    assert const_bucket.value == "#e"
    assert const_bucket.bucket_type == BucketTypes.CONSTANT
    assert const_bucket.constant_type == ConstantTypes.E

    str_bucket = se.Bucket.from_ffi(Bucket_FFI(b"fuzzy", c_int(2), c_int(1)))

    assert str_bucket.value == "fuzzy"
    assert str_bucket.bucket_type == BucketTypes.STRING


def test_engine_signal_construction():
    """Test that EngineSignalSets can be converted from EngineSignalSet_FFI structs."""
    stack_signal = se.EngineSignalSet.from_ffi(
        EngineSignalSet_FFI(stack_updated=True, quit=False, error=b""),
    )

    assert stack_signal.stack_updated()
    assert not stack_signal.should_quit()
    assert not stack_signal.has_error()
    assert not stack_signal.get_error()

    quit_signal = se.EngineSignalSet.from_ffi(
        EngineSignalSet_FFI(stack_updated=False, quit=True, error=b""),
    )

    assert not quit_signal.stack_updated()
    assert quit_signal.should_quit()
    assert not quit_signal.has_error()
    assert not stack_signal.get_error()

    error_signal = se.EngineSignalSet.from_ffi(
        EngineSignalSet_FFI(stack_updated=False, quit=False, error=b"this is an error"),
    )

    assert not error_signal.stack_updated()
    assert not error_signal.should_quit()
    assert error_signal.has_error()
    assert error_signal.get_error() == "this is an error"


def test_submit_multiple_commands():
    """Test the submission of multiple commands to the engine."""
    e = se.SquiidEngine()

    res = e.execute_multiple_rpn(["3", "5", "7", "multiply", "add"])

    assert not res.has_error()

    stack = e.get_stack()
    assert stack[0].value == "38"
    assert stack[0].bucket_type == se.BucketTypes.FLOAT


def test_submit_single_command():
    """Test the submission of a single command to the engine."""
    e = se.SquiidEngine()

    res = e.execute_single_rpn("3")
    res = e.execute_single_rpn("4")
    res = e.execute_single_rpn("add")

    assert not res.has_error()

    stack = e.get_stack()
    assert stack[0].value == "7"
    assert stack[0].bucket_type == se.BucketTypes.FLOAT


def test_get_stack_empty():
    """Test that getting the stack while empty behaves correctly."""
    e = se.SquiidEngine()

    assert e.get_stack() == []


def test_get_commands():
    """Test that we're able to get commands from the engine."""
    e = se.SquiidEngine()

    res = e.get_commands()

    # test some made up constraints
    assert len(res) > 10  # noqa: PLR2004
    assert "add" in res


def test_get_set_previous_answer():
    """Test getting and setting of the previous answer."""
    e = se.SquiidEngine()

    _ = e.execute_multiple_rpn(["3", "4", "5", "multiply", "add"])

    res = e.update_previous_answer()
    assert not res.has_error()

    assert e.get_previous_answer().value == "23"

    _ = e.execute_multiple_rpn(["12", "6", "divide"])

    # make sure it hasnt changed yet
    assert e.get_previous_answer().value == "23"

    assert not e.update_previous_answer().has_error()

    assert e.get_previous_answer().value == "2"
